@extends('posts.master')

@section('content')

	<div class="row">
		<div class="col-lg-12">
			<h3>CRUD練習用</h3>
		</div>
	</div>
	<div class="row">
		<div class="col-sm-12">
			<div class="pull-right">
				<a class="btn btn-xs btn-success" href="{{ route('posts.create') }}">新規作成：Create New Post</a>
			</div>
		</div>
	</div>

	@if ($message = Session::get('success'))
		<div class="alert alert-success">
			<p>{{ $message }}</p>
		</div>
	@endif

	<table class="table table-bordered">
		<tr>
			<th>No.</th>
			<th>タイトル：Title</th>
			<th>本文：Body</th>
			<th width="300px">Actions</th>
		</tr>

		@foreach ($posts as $post)
			<tr>
				<td>{{ ++$i }}</td>
				<td>{{ $post->title }}</td>
				<td>{{ $post->body }}</td>
				<td>
					<a class="btn btn-xs btn-info" href="{{ route('posts.show', $post->id) }}">見る：Show</a>
					<a class="btn btn-xs btn-primary" href="{{ route('posts.edit', $post->id) }}">編集：Edit</a>

					{!! Form::open(['method' => 'DELETE', 'route'=>['posts.destroy', $post->id], 'style'=> 'display:inline']) !!}
					{!! Form::submit('消す：Delete',['class'=> 'btn btn-xs btn-danger']) !!}
					{!! Form::close() !!}
				</td>
			</tr>
		@endforeach
	</table>

	{!! $posts->links() !!}

@endsection
