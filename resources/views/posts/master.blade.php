<!DOCTYPE html>
<html lang="ja">
<head>
	<meta charset="UTF-8">
	<title>タイトル部分(CRUD練習用)</title>
	<link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

	<div class="container">
		@yield('content')
	</div>

<script src="//code.jquery.com/jquery-1.11.2.min.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
</body>
</html>
