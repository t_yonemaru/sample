@extends('posts.master')

@section('content')

	<div class="row">
		<div class="col-lg-12">
			<div class="pull-left">
				<h3>記事を見る</h3>
			</div>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-12">
			<div class="form-group">
				<strong>タイトル : </strong>
				{{ $post->title }}
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-xs-12">
			<div class="form-group">
				<strong>本文 : </strong>
				{{ $post->body }}
			</div>
		</div>
	</div>
	<a class="btn btn-xs btn-primary" href="{{ route('posts.index') }}">戻る：Back</a>

@endsection
